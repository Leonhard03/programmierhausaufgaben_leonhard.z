package test;
import model.Rechteck;

public class RechteckTest {

	public static void main(String[] args) {
	
	//Parameterlose Konstruktoren
	Rechteck R0 = new Rechteck();
	Rechteck R1 = new Rechteck();
	Rechteck R2 = new Rechteck();
	Rechteck R3 = new Rechteck();
	Rechteck R4 = new Rechteck();
	
	//Paramisierte Konstruktoren
	Rechteck R5 = new Rechteck(200,200,200,200);
	Rechteck R6 = new Rechteck(800,400,20,20);
	Rechteck R7 = new Rechteck(800,450,20,20);
	Rechteck R8 = new Rechteck(850,400,20,20);
	Rechteck R9 = new Rechteck(855,455,25,25);

	
	R0.setX(10);
	R0.setY(10);
	R0.setHoehe(30);
	R0.setBreite(40);
	
	R1.setX(25);
	R1.setY(25);
	R1.setHoehe(100);
	R1.setBreite(20);
	
	R2.setX(260);
	R2.setY(10);
	R2.setHoehe(200);
	R2.setBreite(100);
	
	R3.setX(5);
	R3.setY(500);
	R3.setHoehe(300);
	R3.setBreite(25);
	
	R4.setX(100);
	R4.setY(100);
	R4.setHoehe(100);
	R4.setBreite(100);
	
	
	System.out.println(R1.toString());
}
}

