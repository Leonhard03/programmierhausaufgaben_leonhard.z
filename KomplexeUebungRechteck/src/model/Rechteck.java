package model;

public class Rechteck {
	
	// Attribute
	public int x;
	public int y;
	public int hoehe;
	public int breite;	
	{
	
	this.x = 0;
	this.y = 0;
	this.hoehe = 0;
	this.breite = 0;
	}
	
	public Rechteck (int x, int y, int hoehe, int breite) {
	
	this.x = x;
	this.y = y;
	this.hoehe = hoehe;
	this.breite = breite;

	}	
	
	public Rechteck() {}
	
	// Methoden
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = breite;
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = hoehe;
	}

	@Override
	public String toString() {
		return "Rechteck [x=" + x + ", y=" + y + ", hoehe=" + hoehe + ", breite=" + breite + "]";
	}

	
	
}

